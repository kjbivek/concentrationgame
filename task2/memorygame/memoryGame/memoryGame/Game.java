package memoryGame;

import java.util.Random;

public class Game {

	private int realValues[][];
	private int tempBoard[][];
	private int X,Y;																				
	private int scores = 0;
	private int N;
	private int turn;
	
	
	/*
	 * initialize the board with different
	 * values to play the game
	 */
	public Game(int n) {
		
		N = n;
		X = -1;
		Y = -1;
		turn = 0;
		realValues = new int[N][N];
		tempBoard = new int[N][N];
		for(int i=0; i<N; i++)
			for(int j=0; j<N; j++) {
				tempBoard[i][j] = -1;
				realValues[i][j] = -1;
			}
		
		Random rand = new Random();				
		int f = 0;
		int x,y;
		
		for(int i=0; i<(N*N)/2; i++) {
			f = 0;
			while(f!=2) {
				x = rand.nextInt(N);				
				y = rand.nextInt(N);
				if(realValues[x][y]==-1) {			
					f++;
					realValues[x][y] = i;
				}
			}
		}
	}
	
	//win method to check if the game is complete or not
	public boolean win() {					
		for(int i=0; i<N; i++)
			for(int j=0; j<N; j++)
				if(tempBoard[i][j]==-1)				
					return true;
		return false;
	}
	
	
	//  method to play the game for the selected
	 
	public void makeMove(int x, int y) {					
		turn++;
		if(X==-1 && Y==-1) {					
			tempBoard[x][y] = realValues[x][y];		
			X = x;
			Y = y;								
		}
		else if(tempBoard[X][Y] != realValues[x][y]) {			
			tempBoard[X][Y] = -1;					
			X = -1;
			Y = -1;								
			scores -= 5;
			
		}
		else {
			tempBoard[x][y] = realValues[x][y];								
			X = -1;
			Y = -1;							
			scores += 10;
		}
	}
	
	
	public int getPosition(int x, int y) {
		return tempBoard[x][y];
	}
	
	/*
	 * method to return the scores
	 */
	public int getScore() {
		return scores;
	}
	
	public int getTurn() {
		return turn;
	}
	
}
