package memoryGame;


 import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceDialog;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

//GameGUI class created
public class GameGUI extends Application {

	Button[][] block ;		
	int N;
	Game obj;
	GridPane grid;			
	String[] val;
	ImageView image;
	
	//choosing the level
	public int inputLevel() {
		List<String> choices = new ArrayList<>();
		choices.add("Beginner");
		choices.add("Amatuer");
		choices.add("Professional");
		
		ChoiceDialog<String> dialog = new ChoiceDialog<>("Beginer", choices);
		dialog.setTitle("Select Difficulty");
		dialog.setHeaderText("Welcome To The Game!!");
		dialog.setContentText("Please select a level:");
		
		Optional<String> result = dialog.showAndWait();
		if (result.isPresent()){
		    switch(result.get()){
		    case "Beginer":	return 2;
		    case "Amateur":	return 4;
		    case "Professional":	return 6;
		    }
		}
		return 2;
	}
	//consturctor
	public GameGUI() {
		N = inputLevel();
		block = new Button[N][N];
		obj = new Game(N);
		grid = new GridPane();
		val = new String[]{"1.png", "2.png", "3.png", "4.png", "5.png", "6.png", "7.png", "8.png","9.png","10.png","11.png","12.png","13.png","14.png","15.png","16.png","17.png","18.png"};	
	}
	
	public void playGame() {
		int i,j;
		for(i=0; i<N; i++) {
			for(j=0; j<N; j++) {
				block[i][j].setOnAction(myHandler);		
			}
		}
	}
	//event handler
	final EventHandler<ActionEvent> myHandler = new EventHandler<ActionEvent>() {
		
		@Override
		public void handle(ActionEvent event) {					
			// TODO Auto-generated method stub
			int x=0,y=0;
			for(int i=0; i<N; i++)
				for(int j=0; j<N; j++)
					if(event.getSource()== block[i][j]) {			
						x=i; y=j;								
						break;
					}
			if(obj.getPosition(x, y)==-1) {						
				obj.makeMove(x, y);					
	        	display();									
	        	if(!obj.win()) {						
	        		new Alert(Alert.AlertType.INFORMATION, "You won the game with" + obj.getTurn() + " moves with " + obj.getScore() + " scores.").showAndWait();
	        		((Stage)(block[x][y].getScene().getWindow())).close();			
	        	}
			}
				
		}
	};
	//displays images
	public void display() {
		
		for(int i=0; i<N; i++)
			for(int j=0; j<N; j++)
				try{
					if(obj.getPosition(i, j) == -1) {
						block[i][j].setGraphic(null);
					}
						
					else {
							image = new ImageView(new Image(new FileInputStream(val[obj.getPosition(i, j)])));
							image.setFitWidth(100);
							image.setFitHeight(100);
							block[i][j].setGraphic(image);
					}
				}catch (FileNotFoundException e) {
					
					e.printStackTrace();
				}
					
					
	}
	//displays board
	public void initBoard() {
		
		for(int i=0; i<N; i++) {
			for(int j=0; j<N; j++) {
				block[i][j] = new Button();
				block[i][j].setMaxWidth(100);
				block[i][j].setPrefWidth(100);
				block[i][j].setMinWidth(100);
				block[i][j].setMaxHeight(100);
				block[i][j].setPrefHeight(100);
				block[i][j].setMinHeight(100);
				grid.add(block[i][j], i, j);
			}
		}
		display();
		playGame();
	}
	//stars stage
	@Override
	public void start(Stage primaryStage) {
		
		primaryStage.setTitle("Memory Concentration Game");
		
		
		grid.setAlignment(Pos.CENTER);
		
		
		initBoard();
		
		Scene scene = new Scene(grid, 650, 650);
		primaryStage.setScene(scene);
		primaryStage.show();	
		display();
	}

//main method 
	public static void main(String[] args) {
		launch(args);
	}
}
