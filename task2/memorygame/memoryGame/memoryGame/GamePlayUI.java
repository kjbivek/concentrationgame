package memoryGame;

import java.util.Scanner;

import memoryGame.*;

public class GamePlayUI {
	static int N=4;
	static String[] val  = {"1", "2", "3", "4", "5", "6", "7", "8"};		
	static Game obj;

	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner scanner = new Scanner(System.in);
		
		do{
			System.out.println("\nChose the difficulty level.\n1. Beginner\n2. Amature\n3. Professional");
			N = scanner.nextInt();
		}while(N<1 || N>3);
		N = N+2;
		obj = new Game(N);
		print();
		
		
		int x, y;
		int flag = 0;
		
		while(obj.win()) {
			flag = 0;
			do{
				System.out.print("\nEnter the row");
				x = scanner.nextInt() - 1;
				System.out.print("\nEnter the column");
				y = scanner.nextInt() - 1;
				try {if(x>=0 && x<=3 && y>=0 && y<=3) {
					if(obj.getPosition(x, y) == -1)
						flag = 1;
					else 
						System.out.print("\nThis square is already displayed. Select again.");
				}
				else
					System.out.print("\nPlease select a valid square.");}
				catch(ArrayIndexOutOfBoundsException aiobe){
					System.out.println(aiobe.getMessage());
				}
				
			}while(flag!=1);
			
			obj.makeMove(x,y);
			print();
		}
		
		System.out.print("\nYou won the game in " + obj.getTurn() + " turns. Score:" + obj.getScore() );
	}
	
	
	public static void print() {
		
		for(int i=0; i<N; i++) {
			System.out.print("\n--"	+ "");
			seperateLine();
			System.out.print("--\n**\t");
			for(int j=0; j<N; j++)
				if(obj.getPosition(i, j)!= -1)
					System.out.print(val[obj.getPosition(i, j)] + "\t**\t");
				else
					System.out.print("    \t**\t");
		}
		System.out.print("\n--");
		seperateLine();
		System.out.print("--\n");
		
	}
	
	public static void seperateLine() {
		for(int i=0; i<N; i++)
			System.out.print("----------------");
	}
	
	

}
