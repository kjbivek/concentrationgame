package memoryGame;

import static org.junit.Assert.*;

import org.junit.Test;

public class GameTest {

	Game g = new Game(4);
	
	public void gameCompleteTest() {
		assertTrue(g.win());
	}
	
	public void makeMoveTest() {
		g.makeMove(1, 1);
		assertTrue(g.getPosition(1, 1) != -1);
		g.makeMove(2, 2);
		assertTrue(g.getPosition(1, 1) == -1);
		assertTrue(g.getPosition(1, 1) == -1);
	}


}
